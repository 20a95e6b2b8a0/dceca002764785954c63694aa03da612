// SquareLine LVGL GENERATED FILE
// EDITOR VERSION: SquareLine Studio 1.0.5
// LVGL VERSION: 8.2
// PROJECT: SquareLine_Project

#ifndef _SQUARELINE_PROJECT_UI_H
#define _SQUARELINE_PROJECT_UI_H

#ifdef __cplusplus
extern "C" {
#endif

#if __has_include("lvgl.h")
#include "lvgl.h"
#else
#include "lvgl/lvgl.h"
#endif

extern lv_obj_t * ui_Screen1;
extern lv_obj_t * ui_Button1;
extern lv_obj_t * ui_Button2;
extern lv_obj_t * ui_Button3;
extern lv_obj_t * ui_Label2;
extern lv_obj_t * ui_Label3;
extern lv_obj_t * ui_Label4;
extern lv_obj_t * ui_Screen2;
extern lv_obj_t * ui_Keyboard1;
extern lv_obj_t * ui_Image1;
extern lv_obj_t * ui_TextArea1;
extern lv_obj_t * ui_Screen3;
extern lv_obj_t * ui_Image2;
extern lv_obj_t * ui_Label1;


LV_IMG_DECLARE(ui_img_lock_png);    // assets/lock.png
LV_IMG_DECLARE(ui_img_706800910);    // assets/cat-bowing.png




void ui_init(void);

#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif
